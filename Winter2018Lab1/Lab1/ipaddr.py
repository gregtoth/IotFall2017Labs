#!/usr/bin/python
# =============================================================================
#        File : ipaddr.py
# Description : Displays IP addr on the Raspberry Pi SenseHat 8x8 LED display
#               e.g. '10.193.72.242'
#      Author : Drew Gislsason
#        Date : 3/1/2017
# =============================================================================

# To use on bootup of :
# sudo nano /etc/rc.local add line: python /home/pi/ipaddr.py &
#

import socket
import sys


from sense_hat import SenseHat
sense = SenseHat()
sense.set_rotation(180)
sense.show_letter('P')

# give time to get on the Wi-Fi network
import time
time.sleep(10)

# get our local IP address. Requires internet access (to talk to gmail.com).
import socket
s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
s.connect(("gmail.com",80))
addr = s.getsockname()[0]
s.close()

# display that address on the sense had
x = 0
while x in range(0, 5):
   sense.show_message(str(addr))
   x = x + 1



t = sense.get_temperature()
p = sense.get_pressure()
h = sense.get_humidity()
t = round(t, 1)
h = round(h, 1)
msg = "Temperature = %s, Pressure=%s, Humidity=%s" %(t,p,h)
sense.show_message(msg, scroll_speed=0.05)

PORT = 3333

sock = socket.socket(socket.AF_INET, # IPv4 Internet
                        socket.SOCK_DGRAM) # UDP
sock.bind(('', PORT))

while True:
  data, addr2 = sock.recvfrom(1024) # buffer size is 1024 bytes
  print data
  sense.show_message(data)

