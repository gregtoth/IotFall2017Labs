"""
Second Lab for the IoT class
Winter 2018

"""

#!/usr/bin/python

import sys
from flask import *
import socket
from sense_hat import SenseHat


app = Flask(__name__)
sense = SenseHat()

messages = [
    {
        'id': 1,
        'message': u'Buy groceries'
    },
    {
        'id': 2,
        'message': u'Learn Python'
    }
]


#--------------API Routes -------------------

@app.route('/messages1', methods=['GET'])
def get_messages1():
    return jsonify({'messages': messages})

#-------------GET: /message/<data> ------------
# get a particular message 
# curl -i http://192.168.0.5:5000/messages/1
#--------------------------------------------
@app.route('/messages/<int:message_id>', methods=['GET'])
def get_message(message_id):
    message = [message for message in messages if message['id'] == message_id]
    print message
    sense.show_message(str(message))
    return jsonify({'message': message[0]})

#-------------POST------------
# add a message to the structure 
# curl -i -H "Content-Type: application/json" -X POST -d '{"message":"hello world"}' http://192.168.0.5:5000/messages
#--------------------------------------------
@app.route('/messages', methods=['POST'])
def create_message():
    if not request.json or not 'message' in request.json:
        abort(400)
    message = {
        'id': messages[-1]['id'] + 1,
        'message': request.json['message']
    }
    messages.append(message)
    return jsonify({'message': message}), 201


#-------------PUT------------
# update a message to the structure 
# curl -i -H "Content-Type: application/json" -X PUT -d '{"message":"hello world"}' http://192.168.0.5:5000/messages/2
#--------------------------------------------
@app.route('/messages/<int:message_id>', methods=['PUT'])
def update_message(message_id):
    message = [message for message in messages if message['id'] == message_id]
    if not request.json:
        abort(400)
    if 'message' in request.json and type(request.json['message']) != unicode:
        abort(400)
    message[0]['message'] = request.json.get('message', message[0]['message'])
    return jsonify({'message': message[0]})

#-------------DELETE------------
# update a message to the structure 
# curl -i -X DELETE http://192.168.0.5:5000/messages/2  
#--------------------------------------------
@app.route('/messages/<int:message_id>', methods=['DELETE'])
def delete_message(message_id):
    message = [message for message in messages if message['id'] == message_id]
    if len(message) == 0:
        abort(404)
    messages.remove(message[0])
    return jsonify({'result': True})


#-------------GET: /temp ------------
# read the switch input by GET method from curl for example
# curl http://192.168.0.2:5000/temp
#--------------------------------------------
@app.route("/temp",methods=['GET'])
def temp():
    t = sense.get_temperature()
    t = round(t, 1)
    msg = "Temperature = %s \n" %(t)
    return msg


#-------------POST: /sensemessage/<data> ------------
# set the led state  by POST method from curl for example
# curl --data 'message=hello' http://192.168.0.2:5000/sensemessage
#--------------------------------------------
@app.route("/sensemessage",methods=['POST'])
def sensemessage():
    message_data = request.data
    print "SenseMessage:" + message_data
    data = str(request.form['message'])
    print data
    sense.show_message(data)
    return "Led St: \n"





if __name__=='__main__':
    app.run(host='0.0.0.0',debug=True)



