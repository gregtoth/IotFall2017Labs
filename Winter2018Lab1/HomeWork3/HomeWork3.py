"""
Third Homework for the IoT class
Winter 2018

"""

#!/usr/bin/python

import sys
from flask import *
import socket
from sense_hat import SenseHat
import string
import json
import sys
import base64
import binascii
import xml.etree.ElementTree as ET
import simple_pb2


# ============================== Data ====================================
PORT = 5000
content_json      = 'application/json'
content_xml       = 'application/xml'
content_protobufs = 'application/protobuf'

# Content-Types
type_JSON     = 0
type_XML      = 1
type_PROTOBUF = 2

app = Flask(__name__)
sense = SenseHat()

messages = [
    {
        'id': 1,
        'message': u'Buy groceries'
    },
    {
        'id': 2,
        'message': u'Learn Python'
    }
]


#--------------API Routes -------------------

@app.route('/messages1', methods=['GET'])
def get_messages1():
    return jsonify({'messages': messages})







#-------------GET: /message/<data> ------------
# get a particular message 
# curl -i http://192.168.0.2:5000/messages/1
# curl -i http://192.168.0.2:5000/messages/1 -H 'Content-Type: application/json'
#--------------------------------------------
@app.route('/messages/<int:message_id>', methods=['GET'])
def get_message(message_id):

  # assume JSON
  content_type = type_JSON
  if 'Content-Type' in request.headers:
    print "Content-Type: " + request.headers['Content-Type']
    if request.headers['Content-Type'] == content_xml:
      content_type = type_XML
    elif request.headers['Content-Type'] == content_protobufs:
      content_type = type_PROTOBUF
    elif request.headers['Content-Type'] == content_json:
       content_type = type_JSON


  message = [message for message in messages if message['id'] == message_id]
  print message[0]['id']
  print message[0]['message']

  #sense.show_message(str(message))

  # return the number in XML format
  if content_type == type_XML:
    rsp_data = '<message>\n'
    rsp_data = rsp_data + '   <id>' + str(message[0]['id']) + '</id>\n'
    rsp_data = rsp_data + '   <message>' + message[0]['message'] +'</message>\n'
    rsp_data = rsp_data + '</message>\n'

  # return the number in Google Protobuf format
  elif content_type == type_PROTOBUF:
    print "encodig protobufs"
    myvar = simple_pb2.Test1()
    myvar.id = message[0]['id']
    myvar.name = message[0]['message']
    mystr = myvar.SerializeToString()
    rsp_data = base64.b64encode(mystr)

  # assume JSON by default
  else:
    rsp_data = jsonify({'message': message[0]})


  return rsp_data, 200

#-------------POST------------
# add a message to the structure 
# curl -i -H "Content-Type: application/json" -X POST -d '{"message":"hello world"}' http://192.168.0.5:5000/messages
# curl -i -H "Content-Type: application/xml" -X POST -d '<message>hello world</message>' http://192.168.0.2:5000/messages
# curl -i -H "Content-Type: application/protobuf" -X POST -d 'AgtIZWxsbyBXb3JsZA==' http://192.168.0.2:5000/messages
#--------------------------------------------
@app.route('/messages', methods=['POST'])
def create_message():


  # assume JSON
  content_type = type_JSON
  if 'Content-Type' in request.headers:
    print "Content-Type: " + request.headers['Content-Type']
    if request.headers['Content-Type'] == content_xml:
      content_type = type_XML
    elif request.headers['Content-Type'] == content_protobufs:
      content_type = type_PROTOBUF
    elif request.headers['Content-Type'] == content_json:
       content_type = type_JSON

    if content_type == type_JSON:
       message = {
           'id': messages[-1]['id'] + 1,
           'message': request.json['message']
       }
       messages.append(message)
       rsp_data = jsonify({'message': message}), 201
    elif content_type == type_XML:
      data = request.get_data()
      root = ET.fromstring(data)
      if root.tag == 'message':
        print root.text
        new_id = messages[-1]['id'] + 1
        print new_id
        message = {
           'id': new_id,
           'message': root.text
        }
        messages.append(message)
        message2 = [message for message in messages if message['id'] == new_id]
        rsp_data = '<message>\n'
        rsp_data = rsp_data + '   <id>' + str(message2[0]['id']) + '</id>\n'
        rsp_data = rsp_data + '   <message>' + message2[0]['message'] +'</message>\n'
        rsp_data = rsp_data + '</message>\n'
      else:
        rsp_data = "fooBar"
    elif content_type == type_PROTOBUF:
      data = request.get_data()
      mystr = base64.b64decode(data)
      #print len(mystr)
      #i = 0
      #while i < len(mystr):
         #if mystr[i].isalpha():
         #print ord(mystr[i])
         #print str(mystr[i])
         #i = i + 1
      count = ord(mystr[1])
      j = 1
      text = str(mystr[1+j])
      while j < count:
         text = text + str(mystr[2+j])
         j = j + 1
      print text
      new_id = messages[-1]['id'] + 1
      print new_id
      message = {
         'id': new_id,
         'message': text
      }
      messages.append(message)
      myvar = simple_pb2.Test1()
      myvar.id = message['id']
      myvar.name = message['message']
      mystr = myvar.SerializeToString()
      rsp_data = base64.b64encode(mystr)


      #print count
      #rsp_data = "fooBar2"

  return rsp_data, 200


#-------------PUT------------
# update a message to the structure 
# curl -i -H "Content-Type: application/json" -X PUT -d '{"message":"hello world"}' http://192.168.0.2:5000/messages/2
# curl -i -H "Content-Type: application/xml" -X PUT -d '<message>hello world</message>' http://192.168.0.2:5000/messages/2
# curl -i -H "Content-Type: application/protobuf" -X PUT -d 'AgtIZWxsbyBXb3JsZA==' http://192.168.0.2:5000/messages/2
#--------------------------------------------
@app.route('/messages/<int:message_id>', methods=['PUT'])
def update_message(message_id):
  message = [message for message in messages if message['id'] == message_id]

  # assume JSON
  content_type = type_JSON
  if 'Content-Type' in request.headers:
    print "Content-Type: " + request.headers['Content-Type']
    if request.headers['Content-Type'] == content_xml:
      content_type = type_XML
    elif request.headers['Content-Type'] == content_protobufs:
      content_type = type_PROTOBUF
    elif request.headers['Content-Type'] == content_json:
       content_type = type_JSON

  if content_type == type_JSON:
    message[0]['message'] = request.json.get('message', message[0]['message'])
    message2 = [message for message in messages if message['id'] == message_id]
    #rsp_data = jsonify({'message': message2[0]}), 201
    rsp_data = "fooBar"
  elif content_type == type_XML:
    data = request.get_data()
    root = ET.fromstring(data)
    if root.tag == 'message':
      message[0]['message'] = root.text
      message2 = [message for message in messages if message['id'] == message_id]
      rsp_data = '<message>\n'
      rsp_data = rsp_data + '   <id>' + str(message2[0]['id']) + '</id>\n'
      rsp_data = rsp_data + '   <message>' + message2[0]['message'] +'</message>\n'
      rsp_data = rsp_data + '</message>\n'
    else:
      rsp_data = "fooBar"
  elif content_type == type_PROTOBUF:
    data = request.get_data()
    mystr = base64.b64decode(data)
    count = ord(mystr[1])
    j = 1
    text = str(mystr[1+j])
    while j < count:
      text = text + str(mystr[2+j])
      j = j + 1
    print text
    message[0]['message'] = text
    message2 = [message for message in messages if message['id'] == message_id]
    myvar = simple_pb2.Test1()
    myvar.id = message2[0]['id']
    myvar.name = message2[0]['message']
    mystr = myvar.SerializeToString()
    rsp_data = base64.b64encode(mystr)

  return rsp_data, 200

#-------------DELETE------------
# update a message to the structure 
# curl -i -X DELETE http://192.168.0.5:5000/messages/2  
#--------------------------------------------
@app.route('/messages/<int:message_id>', methods=['DELETE'])
def delete_message(message_id):
    message = [message for message in messages if message['id'] == message_id]
    if len(message) == 0:
        abort(404)
    messages.remove(message[0])
    return jsonify({'result': True})


#-------------GET: /temp ------------
# read the switch input by GET method from curl for example
# curl http://192.168.0.2:5000/temp
#--------------------------------------------
@app.route("/temp",methods=['GET'])
def temp():
    t = sense.get_temperature()
    t = round(t, 1)
    msg = "Temperature = %s \n" %(t)
    return msg


#-------------POST: /sensemessage/<data> ------------
# set the led state  by POST method from curl for example
# curl --data 'message=hello' http://192.168.0.2:5000/sensemessage
#--------------------------------------------
@app.route("/sensemessage",methods=['POST'])
def sensemessage():
    message_data = request.data
    print "SenseMessage:" + message_data
    data = str(request.form['message'])
    print data
    sense.show_message(data)
    return "Led St: \n"





if __name__=='__main__':
    app.run(host='0.0.0.0',debug=True)



