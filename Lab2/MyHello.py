"""
First Lab for the IoT class
Fall 2017

"""

#!/usr/bin/python

import sys
from flask import Flask
import socket


if socket.gethostname().find('.') >= 0:
    hostname=socket.gethostname()
else:
    hostname=socket.gethostbyaddr(socket.gethostname())[0]

app = Flask(__name__)

@app.route("/")


def hello():
    return "Hello from RaspberryPi: " + hostname





if __name__=='__main__':
    app.run(host='0.0.0.0')

