"""

Fall 2017

"""

#!/usr/bin/python

import sys
import time
from gpio import PiGpio

pi_gpio = PiGpio()

print('Blink all the LEDs in sequence (Ctrl-c to stop)')

while True:

    switch = pi_gpio.read_switch()
    print('\n====Switch: {0}====='.format(switch))

    print('\nLed 1 (RED) on')
    pi_gpio.set_led(1,True)
    print('Led 1: {0}'.format(pi_gpio.get_led(1)))
    print('Led 2: {0}'.format(pi_gpio.get_led(2)))
    print('Led 3: {0}'.format(pi_gpio.get_led(3)))
    time.sleep(1.0)

    print('\nLed 2 (GREEN) on')
    pi_gpio.set_led(2,True)
    print('Led 1: {0}'.format(pi_gpio.get_led(1)))
    print('Led 2: {0}'.format(pi_gpio.get_led(2)))
    print('Led 3: {0}'.format(pi_gpio.get_led(3)))
    time.sleep(1.0)

    print('\nLed 3 (BLUE) on')
    pi_gpio.set_led(3,True)
    print('Led 1: {0}'.format(pi_gpio.get_led(1)))
    print('Led 2: {0}'.format(pi_gpio.get_led(2)))
    print('Led 3: {0}'.format(pi_gpio.get_led(3)))
    time.sleep(1.0)

    print('\nLed 1 (RED) off')
    pi_gpio.set_led(1,False)
    print('Led 1: {0}'.format(pi_gpio.get_led(1)))
    print('Led 2: {0}'.format(pi_gpio.get_led(2)))
    print('Led 3: {0}'.format(pi_gpio.get_led(3)))
    time.sleep(1.0)

    print('\nLed 2 (GREEN) off')
    pi_gpio.set_led(2,False)
    print('Led 1: {0}'.format(pi_gpio.get_led(1)))
    print('Led 2: {0}'.format(pi_gpio.get_led(2)))
    print('Led 3: {0}'.format(pi_gpio.get_led(3)))
    time.sleep(1.0)

    print('\nLed 3 (BLUE) off')
    pi_gpio.set_led(3,False)
    print('Led 1: {0}'.format(pi_gpio.get_led(1)))
    print('Led 2: {0}'.format(pi_gpio.get_led(2)))
    print('Led 3: {0}'.format(pi_gpio.get_led(3)))
    time.sleep(1.0)



